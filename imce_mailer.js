function mailFiles(selectedFiles) {
  /* hide previous mesages */
  $('#mail-error').hide();
  $('#file-error').hide();
  $('#mail-success').hide();
  /* validate data */
  var emails = $('#imce_email_addresses').val(); 
  if (emails == '') {
    $('#mail-error').show();
    imce.setMessage('Please enter an email address to send the files to.', 'error');
    return;
  } 
  else if (!validateEmailField(emails)) {
    $('#mail-error').show();
    imce.setMessage('Enter valid email address, or emails separated by commas.', 'error');
    return;
  }
  if (isEmpty(selectedFiles)) {
    $('#file-error').show();
    imce.setMessage('Please click on the files you\'re emailing below first', 'error');
    return;
  }
  maildata = setupMailData(selectedFiles, emails);
  $.ajax({
    type: 'POST',
    url: Drupal.settings.imce_mailer.base_url + '/imce_mailer',
    success: function(data) {
      if (data.msg == "success") {
        imce.setMessage('Email sent!');
        $('#mail-success').show();
      } 
      else {
        imce.setMessage('Email could not be sent: ' + data.msg, 'error');
      }
    },
    error: function(blah,textStatus) {
      imce.setMessage('Email could not be sent: ' + textStatus, 'error');
    },
    dataType: 'json',
    data: maildata
  });

  return false;
}

function setupMailData(filedata, emails) {
  var mailstr = new Array();
  var i = 0;
  for (var fname in filedata) {
    i = i + 1;
    file = imce.fileGet(fname);
    mailstr = mailstr + 'fname' + i + '=' + encodeURIComponent(file.name) + '&'; 
    mailstr = mailstr + 'url' + i + '=' + encodeURIComponent(file.url) + '&'; 
  }
  mailstr = mailstr + 'emails=' + encodeURIComponent(emails) + '&'; 
  mailstr = mailstr + 'mailertoken=' + $('#mailertoken').attr('value');
  return mailstr;
}

function isEmpty(ob) {
  for(var i in ob) { 
    return false;
  }
  return true;
}

function validateEmailField(email_field) {
  var email = email_field.split(',');
  for (var i = 0; i < email.length; i++) {
    if (!validateEmail(email[i], 1, 0)) {
      return false;
    }
  }
  return true;
}

function validateEmail(x) {
  var atpos=x.indexOf("@");
  var dotpos=x.lastIndexOf(".");
  if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
    return false;
  }
  return true;
}
