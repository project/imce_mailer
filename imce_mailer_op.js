imce.hooks.load.push(function() {
    imce.opAdd({
      name : 'mailer',
      title : 'Email Selected Files',
      content : '<div id="imce_mailer"><span id="imce_mailer_button"></span><span id="mail-error">* </span><label for="imce_email_addresses">Email To:</label><input id="imce_email_addresses" name="imce_email_addresses" type="text" value="" /><p>Email files to multiple recipients by separating email addresses<br /> with commas (ie: joe@example.com, bob@example.com).</p><input type="button" id="imce_send_mail" class="imce_mailer_button form-submit" value="Email Selected Files" onClick="mailFiles(imce.selected);"><input id="mailertoken" type="hidden" value="' + Drupal.settings.imce_mailer.token + '" /><p id="file-error">Click on the file you want to email first.</p><p id="mail-success">Files emailed successfully.</p></div>'
    });
});

