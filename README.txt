IMCE Mailer
-----------

USAGE
-----
This module allows you to share files via email using the IMCE file browser.
Files are not attached in the email, they are simply linked to.

From the IMCE file browser, upload some files.  Select one or more files and
click "Email Selected Files" at the top.  Enter an email address, or a number
of email addresses separated by commas.  Click "Email Selected Files" below
the email address field, and a list of links to the selected files will
be sent to the email addresses you specified.

The contents of the email can be configured from admin/settings/imce_mailer

See INSTALL.txt for installation and configuration instructions
